<?php
require_once("../../conexao.php");
@session_start();

//VERIFICAR SE O USUÁRIO LOGADO É UM ADMINISTRADOR
if (@$_SESSION['nivel_usuario'] != 'Administrador') {
    echo "<script language='javascript'>window.location='../index.php'</script>";
}

/*
echo 'Painel Administrativo <p>';
echo '<br>Nome de Usuário: ' . $_SESSION['nome_usuario'];
echo '<br>Nível do Usuário: ' . $_SESSION['nivel_usuario'];

*/

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Painel Administrativo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">Administrador</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Sair
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="#">
                                    <?php echo $_SESSION['nome_usuario'] ?></a>
                            </li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="../logout.php">Sair</a></li>
                        </ul>
                    </li>

                </ul>
                <form method="GET" class="d-flex">
                    <input class="form-control me-2" type="search" placeholder="Buscar" aria-label="Search" name="txtBuscar">
                    <button class="btn btn-secondary" type="submit">Buscar</button>
                </form>
            </div>
        </div>
    </nav>

    <div class="container">
        <a href="index.php?funcao=novo" class="btn btn-secondary mt-4" type="button">Novo Usuário</a>

        <?php
        $txtBuscar = '%' . @$_GET['txtBuscar'] . '%';
        $query = $pdo->prepare("SELECT * FROM usuarios 
        WHERE nome like :nome or email like :email");
        $query->bindValue(":nome", $txtBuscar);
        $query->bindValue(":email", $txtBuscar);
        $query->execute();
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        $total_reg = @count($res);

        if ($total_reg > 0) {
        ?>
            <table class="table table-stripped mt-4">
                <thead>
                    <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Senha</th>
                        <th scope="col">Nível</th>
                        <th scope="col">Ações</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    for ($i = 0; $i < $total_reg; $i++) {
                        foreach ($res[$i] as $key => $value) {
                        }
                        $id = $res[$i]['id'];
                        $nome = $res[$i]['nome'];
                        $email = $res[$i]['email'];
                        $senha = $res[$i]['senha'];
                        $nivel = $res[$i]['nivel'];
                    ?>
                        <tr>
                            <td><?php echo $nome ?></td>
                            <td><?php echo $email ?></td>
                            <td><?php echo $senha ?></td>
                            <td><?php echo $nivel ?></td>
                            <td>
                                <a href="index.php?funcao=editar&id=<?php echo $id; ?>" title="Editar Registro" class="mr-1">
                                    <i class="bi bi-pencil-square text-primary"></i>
                                </a>
                                <a href="index.php?funcao=excluir&id=<?php echo $id; ?>" title="Excluir Registro">
                                    <i class="bi bi-trash3-fill text-danger"></i>
                                </a>
                            </td>
                        </tr>
                <?php
                    }
                } else {
                    echo '<p>Nenhum usuário encontrado!</p>';
                }
                ?>
                </tbody>
            </table>
    </div>
</body>

</html>

<div class="modal fade" id="modalCadastrar" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                if (@$_GET['funcao'] == 'editar') {
                    $titulo_modal = "Editar Registro";
                } else {
                    $titulo_modal = "Inserir Registro";
                }
                ?>
                <h5 class="modal-title"><?php echo $titulo_modal; ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="form" action="" method="POST">
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <label for="username">Nome:</label><br>
                        <input type="text" name="nomeCad" class="form-control" placeholder="Insira seu Nome" required="">
                    </div>
                    <div class="form-group mb-3">
                        <label for="username">Email:</label><br>
                        <input type="text" name="emailCad" class="form-control" placeholder="Insira seu Email" required="">
                    </div>
                    <div class="form-group mb-3">
                        <label for="password">Senha:</label><br>
                        <input type="password" name="senhaCad" class="form-control" placeholder="Insira sua Senha" required="">
                    </div>
                    <div class="form-group mb-3">
                        <label for="password">Nível:</label><br>
                        <select class="form-select" aria-label="Default select example" name="nivelCad">
                            <option selected>Escolha o nível</option>
                            
                            <option value="Cliente">Cliente</option>
                            <option value="Administrador">Administrador</option>
                            <option value="Vendedor">Vendedor</option>
                            <option value="Tesoureiro">Tesoureiro</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button name="btn-cadastrar" type="submit" class="btn btn-primary">Salvar</button>
                    </div>
            </form>
        </div>
    </div>
</div>

<?php 

if (isset($_POST['btn-cadastrar'])) { 
    $query_v = $pdo->prepare("SELECT * FROM usuarios WHERE email = :email");
    $query_v->bindValue(":email", $_POST['emailCad']);
    $query_v->execute();
    $res_v = $query->fetchAll(PDO::FETCH_ASSOC);
    $total_reg_v = @count($res);

    if ($total_reg_v > 0) {
        echo "<script language='javascript'>window.alert('Email já cadastrado!')</script>";
        exit();
    }
    $query = $pdo->prepare("INSERT INTO usuarios (nome, email, senha, nivel)
     VALUES (:nome, :email, :senha, :nivel)");
    $query->bindValue(":nome", $_POST['nomeCad']);
    $query->bindValue(":email", $_POST['emailCad']);
    $query->bindValue(":senha", $_POST['senhaCad']);
    $query->bindValue(":nivel", $_POST['nivelCad']);
    $query->execute();

    echo "<script language='javascript'>window.alert('Cadastrado com sucesso')</script>";
    echo "<script language='javascript'>window.location='index.php'</script>";
}
?>

<?php
if (@$_GET['funcao'] == 'novo') { ?>
    <script>
        var myModal = new bootstrap.Modal(document.getElementById('modalCadastrar'), {
            keyboard: false
        });
        myModal.show();
    </script>
<?php } ?>

<?php
if (@$_GET['funcao'] == 'editar') { ?>
    <script>
        var myModal = new bootstrap.Modal(document.getElementById('modalCadastrar'), {
            keyboard: false
        });
        myModal.show();
    </script>
<?php } ?>

<?php if (@$_GET['funcao'] == 'excluir') {
}
?>